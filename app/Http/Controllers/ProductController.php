<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductShowResource;
use App\models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return ProductShowResource::collection(Product::all());

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Product::create([
            'name' => $request->name,
            'qty' => $request->qty
        ]);
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
      return new ProductShowResource($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
        return response([

            'data'=>$product->get(),
            'status'=>true

        ]);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
      $product->delete();
    }
}